# Security
- A Federation is a group of computing or network providers agreeing upon standards of operation in a collective fashion. The term may be used when describing the inter-operation of two distinct, formally disconnected, telecommunications networks that may have different internal structures.
- SAML (Secure Assertion Markup Language) is an open standard for exchanging authentication and authorization data between parties, in particular, between an IdP (identity provider) and a service provider.
- LDAP (Lightweight Directory Access Protocol) is an open, vendor-neutral, industry standard application protocol for accessing and maintaining distributed directory information services over an Internet Protocol (IP) network.
- ARNs (Amazon Resource Names) uniquely identify AWS resources. ARNs are requiren when you need to specify a resource unambiguously across all of AWS, such as in IAM policies, Amazon Relational Database Service (Amazon RDS) tags, and API calls.

## IAM (Identity and Access Management)
- Securely control and access AWS services and resources for your users.
- Can set permissions for users or groups.
- New users are created by default with no permissions.
- New provisions are applied immediately.
- Can enable DB Authentication to authenticate a DB instance.
- Resources:
    - [IAM Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-aws-identity-and-access-management-iam/)

### IAM Groups
- Lets you specify permissions for multiple users, which can make it easier to manage the permissions for those users.

### IAM Cross-Account Access
- IAM feature

## IAM DB Authentication
- Authenticate a DB instance via an authentication token instead of a username and password.
- Works with MySQL and PostgreSQL.
- Use IAM to centrally manage access to your database resources, instead of managing access individually on each DB instance.
- Resources:
    - [IAM Database Authentication for MySQL and PostgreSQL](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/UsingWithRDS.IAMDBAuth.html)

## KMS (Key Management Service)
- Managed service that makes it easy for you to create and control the encryption keys used to encrypt your data.
- The master keys that you create in AWS KMS are protected by FIPS 140-2 validated cryptographic modules.
- Integrated with AWS CloudTrail to provide encryption key usage logs to help meet your auditing, regulatory and compliance needs.

## Organizations
- Account management service that lets you consolidate multiple AWS accounts into an organization that you create and centrally manage.
- Use to create member accounts and invite existing accounts to join your organization.
- Accounts can be organized into groups and have policy-based controls attached to them.

## Directory Service
- Manage workload access from Microsoft AD (Active Directory).
- Directory types include AD Connector, Simple AD, Directory Service for Microsoft AD, Cloud Directory, and Cognito.
- Resources:
    - [Directory Service Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-aws-directory-service/)

### Directory Service AD Connector
- Proxy service that provides an easy way to connect compatible AWS applications, such as WorkSpaces, QuickSight, and EC2 for Windows Server instances, to your existing on-premises Microsoft AD.
- Resources:
    - [How to Connect Your On-Premises Active Directory to AWS Using AD Connector](https://aws.amazon.com/blogs/security/how-to-connect-your-on-premises-active-directory-to-aws-using-ad-connector/)

### Directory Service Simple AD
- Provides a subset of the features offered by AWS Managed Microsoft AD, including the ability to manage user accounts and group memberships, create and apply group policies, securely connect to Amazon EC2 instances, and provide Kerberos-based single sign-on (SSO).
- Microsoft AD–compatible directory from AWS Directory Service that is powered by Samba 4.
- Supports basic Active Directory features such as user accounts, group memberships, joining a Linux domain or Windows based EC2 instances, Kerberos-based SSO, and group policies.

### Directory Service for Microsoft AD
- Enables migration of a broad range of Active Directory–aware applications to the AWS Cloud. Powered by an actual Microsoft Windows Server AD and managed by AWS in the AWS Cloud.

### Directory Service Cloud Directory
- Cloud-native directory that can store hundreds of millions of application-specific objects with multiple relationships and schemas. Useful if you need a highly scalable directory store for your application’s hierarchical data.

### Directory Service Cognito
- User directory that adds sign-up and sign-in to your mobile app or web application using Cognito User Pools. Useful when you need to create custom registration fields and store that metadata in your user directory.

## RAM (Resource Access Manager)
- Share AWS resources (e.g. subnets or License Manager configurations) with any AWS account or within your AWS Organization.
- Eliminates the need to create duplicate resources in multiple accounts, reducing the operational overhead of managing those resources in every single account you own.
- Use to create a Resource Share, specify resources, and specify accounts.
- Resources:
    - https://docs.aws.amazon.com/ram/latest/userguide/shareable.html

## Control Tower
- Set up and govern a new, secure, multi-account AWS environment.
- Resources:
    - https://aws.amazon.com/controltower/

## Shield
- Managed DDoS (Distributed Denial of Service) protection service that safeguards apps running on AWS.
- Standard Tier is enabled by default but can upgrade to advanced for more protection.
- Resources:
    - [AWS Shield Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-aws-shield/)

### Shield Standard
- Enabled by default, protects against most common network and transport layer attacks.

### Shield Advanced
- Higher level of protection against more sophisticated attacks targeting apps on EC2, ELB, CloudFront, Global Accelerator, and Route 53. 
- Can integrate with WAF (Web Application Firewall). Gives access to DRT (DDoS Response Team). Can also be enabled from Elastic IP.

## STS (Security Token Service)
- Service that enables you to request temporary, limited-privilege credentials for AWS Identity and Access Management (IAM) users or for users that you authenticate (federated users).
- Resources:
    - [Security Token Service](https://docs.aws.amazon.com/STS/latest/APIReference/Welcome.html)

## Secrets Manager
- Protect secrets needed to access your applications, services, and IT resources.
- Enables you to easily rotate, manage, and retrieve database credentials, API keys, and other secrets throughout their lifecycle.
- Users and applications retrieve secrets with a call to Secrets Manager APIs, eliminating the need to hardcode sensitive information in plain text.

## Systems Manager
- Centralizes operational data from multiple AWS services and automate tasks across your AWS resources.
- Provides a central place to view and manage your AWS resources, so you can have complete visibility and control over your operations.
- Can create logical groups of resources such as applications, different layers of an application stack, or production versus development environments.
- Can select a resource group and view its recent API activity, resource configuration changes, related notifications, operational alerts, software inventory, and patch compliance status.
- Resources:
    - [Systems Manager Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-aws-systems-manager/)
    - [Systems Manager](https://aws.amazon.com/systems-manager/)

### Systems Manager Parameter Store
- Centralized store to manage your configuration data, whether plain-text data such as database strings or secrets such as passwords.
- Allows you to separate your secrets and configuration data from your code.
- Parameters can be tagged and organized into hierarchies, helping you manage parameters more easily.
- Integrated with AWS Key Management Service (KMS), allowing you to automatically encrypt the data you store.
- Can also control user and resource access to parameters using AWS Identity and Access Management (IAM).
- Parameters can be referenced through other AWS services, such as ECS, Lambda, and CloudFormation.
- Resources:
    - [The Right Way to Store Secrets using Parameter Store](https://aws.amazon.com/blogs/mt/the-right-way-to-store-secrets-using-parameter-store/)
