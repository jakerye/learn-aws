# Storage
- Resources:
    - [S3 vs EBS vs EFS Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-s3-vs-ebs-vs-efs/)

## S3 (Simple Storage Service)
- Can use S3 Versioning and Multi-Factor Authentication Delete to prevent accidental deletion of key data.
- Can use CORS (Cross Origin Resource Sharing) to enable client web applications that are loaded in one domain to interact with resources in a different domain.
- Configs include: On-Demand, Glacier, Lake?, Standard-IA, One Zone-IA
- Resources:
    - [S3 Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-s3/)
    - [Protecting Data Using Encrypyion](https://docs.aws.amazon.com/AmazonS3/latest/dev/UsingEncryption.html)
    - [One Zone-IA](https://aws.amazon.com/s3/storage-classes/#Amazon_S3_One_Zone-Infrequent_Access)
    - [Using Versioning](https://docs.aws.amazon.com/AmazonS3/latest/dev/Versioning.html)

### S3 CORS (Cross-Origin Resource Sharing)
- Defines a way for client web applications that are loaded in one domain to interact with resources in a different domain.
- With CORS support, you can build rich client-side web applications with Amazon S3 and selectively allow cross-origin access to your Amazon S3 resources.
- Resources:
    - [Cross-Origin Resource Sharing](https://docs.aws.amazon.com/AmazonS3/latest/dev/cors.html)
    - [Enabling Cross-Origin Resource Sharing](https://docs.aws.amazon.com/AmazonS3/latest/dev/ManageCorsUsing.html)

### S3 CRR (Cross-Region Replication)
- A bucket-level configuration that enables automatic, asynchronous copying of objects across buckets in different AWS Regions.

### S3 Select
- S3 feature that makes it easy to retrieve specific data from the contents of an object using simple SQL expressions without having to retrieve the entire object.
- Can use to reduce cost and latency of data requests.
    
## EBS (Elastic Block Store)
- Persistent block storage for use with EC2.
- Automatically replicated within AZ (Availability Zone).
- Supports autoscaling up to PB.
- Workloads include relational DB, non-relational DB, enterprise apps, containerized apps, big data analytics, file systems, and media.
- Can use EBS Encryption to straight-forwardly encrypt EBS resources without having to manage an encryption app.
- Features: Snapshots, ...
- Volume Types:
    - io1 (Provisioned IOPS SSD): Ideal for latency-sensitive transactional workloads. Useful for I/O-intensive NoSQL & relational databases.
    - gp2 (General Purpose SSD): Default volume for transactional workloads with single-digit millisecond latency. Useful for Boot volumes, low-latency interactive apps, dev & test.
    - st1 (Throughput Optimized HDD): Low cost HDD volume designed for frequently accessed, throughput-intensive workloads. Useful for big data, data warehouse, and log processing. 
    - sc1 (Cold HDD):  Lowest cost HDD volume designed for less frequently accessed workloads. Useful for colder data requiring fewer scans per day.
- Resources:    
    - [EBS Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-ebs/)

### EBS Snapshots
- Can use to periodically backup S3 volumes. 
- The replicated volume loads data in the background so that you can begin using it immediately. 
- If you access data that hasn't been loaded yet, the volume immediately downloads the requested data from Amazon S3, and then continues loading the rest of the volume's data in the background. 
- Although you can take a snapshot of a volume while a previous snapshot of that volume is in the pending status, having multiple pending snapshots of a volume may result in reduced volume performance until the snapshots complete.
- Resources:
    - [Creating EBS Snapshots](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-creating-snapshot.html)


## EFS (Elastic File Store)
- Fully-managed file storage service.
- Resources:
    - [Amazon EFS Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-efs/)
