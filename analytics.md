## Analytics

## Kinesis
- Ingest and analyze real-time streaming data (e.g. telemetry, logs, clickstreams, audio, video).
- Resources:
    - [Kinesis Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-kinesis/)

### Kinesis Data Firehose
- Fully-managed service for loading streaming data into data stores and analytics tools including S3, Redshift, Elasticsearch Service, and Splunk.
- Can batch, compress, and encrypt data before loading it, minimizing the amount of storage used at the destination and increasing security.
- Resources:
    - [Kinesis Data Firehose](https://aws.amazon.com/kinesis/data-firehose/)

## Redshift
- Data warehouse to analyze data in warehouse and S3 data lake.
- Features: AQUA, Spectrum, Enhanced VPC Routing, Audit Logging, ...
- Can use AQUA (Advanced Query Acclerator) -- a distributed and hardware-accelerated cache -- to increase performance by 10x.
- Can use the WLM (Workload Management) in the parameter group configuration to define the number of query queues that are available and how queries are routed to those queues for processing.
- Can enable Audit Logging to get the information about the connection, queries, and user activities in your Redshift cluster.
- Resources:
    - [Redshift Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-redshift/)
    - [Configuring Workload Management](https://docs.aws.amazon.com/redshift/latest/mgmt/workload-mgmt-config.html)

### Redshift Spectrum
- Redshift feature that enables you to run queries against exabytes of unstructured data in Amazon S3 with no loading or ETL required.
- Can use to query data directly from files in Amazon S3.

## Redshift Enhanced VPC Routing
- Redshift feature that enables you to force all of COPY and UNLOAD traffic between your cluster and your data repositories through your VPC (Virtual Private Cloud).
- By using Enhanced VPC Routing, you can use standard VPC features, such as VPC security groups, network access control lists (ACLs), VPC endpoints, VPC endpoint policies, internet gateways, and Domain Name System (DNS) servers.
- Resources:
    - [Enhanced VPC Routing](https://docs.aws.amazon.com/redshift/latest/mgmt/enhanced-vpc-routing.html)

## Elasticsearch Service
- Managed elasticsearch.
- Use to search, analyze, and visualize data in real-time.
- Resources:
    -[Elasticsearch Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-elasticsearch-amazon-es/)

## Athena
- Interactive query service that makes it easy to analyze data in Amazon S3 using standard SQL.
- Resources:
    - [Athena Cheat Sheet](https://tutorialsdojo.com/amazon-athena/)

## EMR
- A partially-managed cluster platform that simplifies running big data frameworks, such as Apache Hadoop and Apache Spark to process and analyze vast amounts of data.
- Can use to transform and move large amounts of data into and out of other AWS data stores and databases.
- Compatible with Apache Hive and Apache Pig.
- Provides you a managed Hadoop framework that makes it easy, fast, and cost-effective to process vast amounts of data across dynamically scalable Amazon EC2 instances.
- Can access the operating system of these EC2 instances that were created by Amazon EMR.
- Resources:
    - [EMR Cheat Sheet](https://tutorialsdojo.com/amazon-emr/)
