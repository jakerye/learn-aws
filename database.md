# Database
- Relational (or SQL) Database:
    - Best for OLAP (Online Analytical Processing)
    - Rigid schema.
    - Useful to support complex queries which fetch data across a number of tables.
- Non-Relational (or NoSQL) Database
    - Best for OLTP (Online Transactional Processing)
    - Flexible schema.
    - Can store complex hierarchical data within a single item.

## ElastiCache
- Managed in-memory cache to boost performance of existing databases to achieve sub-millisecond response times.
- Useful for real-time use cases like Caching, Session Stores, Gaming, Geospatial Services, Real-Time Analytics, and Queuing.
- Supports Redis and Memcached.
- Resources:
    - [ElastiCache Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-elasticache/)
    - [ElastiCache](https://aws.amazon.com/elasticache/)
    - [Redis vs Memcached](https://tutorialsdojo.com/aws-cheat-sheet-redis-cluster-mode-enabled-vs-disabled-vs-memcached/)

### Redis ElastiCache
- Using the AUTH command can improve data security by requiring the user to enter a password before they are granted permission to execute Redis commands on a password-protected Redis server.
- Can use the At-Rest Encryption feature to secure the data inside the in-memory data store.
- Can use the In-Transit Encryption feature to secure the data in transit.
- Resources:
    - https://docs.aws.amazon.com/AmazonElastiCache/latest/red-ug/encryption.html
    - https://docs.aws.amazon.com/AmazonElastiCache/latest/red-ug/auth.html

## DynamoDB
- Fully-managed NoSQL database.
- Supports key-value and document data models.
- Delivers single-digit millisecond performance at any scale. 
- Can handle 10T req/day.
- Can support peaks of 20M req/sec.
- Can increase performance by 10x (from milliseconds to microseconds) with DAX (DynamoDB Acclerator) -- an in memory cache.
- Can enable DynamoDB Streams to capture changes to items stored in a table. Great for triggering Lambda.
- Resources:
    - [DynamoDB Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-dynamodb/)
    - [Capturing Table Activity with DynamoDB Streams](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Streams.html)
    - [Process New Items with DynamoDB Streams and Lambda](https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Streams.Lambda.Tutorial.html)

### DAX (DynamoDB Accelerator)
- Fully-managed, highly-available, in-memory cache that can reduce DynamoDB response times from milliseconds to microseconds.
- Resources:
    - [DynamoDB Accelerator](https://aws.amazon.com/dynamodb/dax/)

## Neptune
- Managed graph database service.
- Makes it easy to build and run applications that work with highly connected dataset.
- Supports Property Graph and W3C's RDF.

## RDS (Relational Database Service)
- Fully-managed database supporting the following engines: Aurora, PostgreSQL, MySQL, MariaDB, Oracle Database, and SQL Server.
- Can use Data Migration Service to ease migrations.
- Can enable Enhanced Monitoring to see how different processes or threads on a DB instance use the CPU.
- Can use Schema Conversion Tool to convert an existing database schema from one database engine to another.
- Can enable Multi-AZ Failover to improve access robustness.
- Can enable Read Replica to provide an asynchronous data replication.
- Resources:
    - [Relational Database Service Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-relational-database-service-amazon-rds/)
    - [RDS Multi-AZ](https://aws.amazon.com/rds/details/multi-az/)

## Aurora
- Managed relational database compatible with MySQL and PostgreSQL.
- 5X throughput of MySQL and 3x throughput PostgreSQL.
- Up to 64TB/instance.
- Typically involves a cluster of DB instances instead of a single instance.
- When you connect to an Aurora cluster, the host name and port that you specify point to an intermediate handler called an endpoint.
- Aurora uses the endpoint mechanism to abstract these connections. Thus, you don't have to hardcode all the hostnames or write your own logic for load-balancing and rerouting connections when some DB instances aren't available.
- Different instances or groups of instances perform different roles.
- The primary instance handles all data definition language (DDL) and data manipulation language (DML) statements.
- Up to 15 Aurora Replicas handle read-only query traffic
- For clusters with DB instances of different capacities or configurations, you can connect to custom endpoints associated with different subsets of DB instances. For example, you might direct internal users to low-capacity instances for report generation or ad hoc (one-time) querying, and direct production traffic to high-capacity instance.
- Resources:
    - [Aurora Cheat Sheet](https://tutorialsdojo.com/amazon-aurora/)
    - [Amazon Aurora Connection Management](https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/Aurora.Overview.Endpoints.html)
