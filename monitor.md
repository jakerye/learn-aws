# Application Monitoring

## CloudWatch
- Application monitoring that collects logs, metrics, and events to detect anomalous behavior, set alarms, visualize logs & metrics, take automated actions, troubleshoot issues, and discover insights to keep apps running smoothly.
- Readily available metrics include CPU Utilization (the processing power required to run an application upon a selected instance), Network Utilization (the volume of incoming and outgoing network traffic to a single instance), and Disk Reads (the volume of the data the application reads from the hard disk of the instance, can be used to determine the speed of the application).
- For non-readily available metrics can either install a CloudWatch Agent or write a Cloud Watch Monitoring Script (written in perl).
- Resources:
    - https://tutorialsdojo.com/aws-cheat-sheet-amazon-cloudwatch/

### CloudWatch Agent
- Use to monitor the following non-readily available metrics: 
    - Memory Utilization
    - Disk Swap Utilization
    - Disk Space Utilization
    - Page File Utilization
    - Log Collection
- Resources:
    - [CloudWatch Agent vs SSM Agent vs Custom Daemon Scripts](https://tutorialsdojo.com/aws-cheat-sheet-cloudwatch-agent-vs-ssm-agent-vs-custom-daemon-scripts/)

## Enhanced Monitoring
- Gathers its metrics from an agent on the instance as opposed to CloudWatch which gathers metrics about CPU utilization from the hypervisor for a DB instance.
- Useful when you want to see how different processes or threads on a DB instance use the CPU.

## CloudTrails
- Enables governance, compliance, operational auditing, and risk auditing of your AWS account.
- Log, continuously monitor, and retain account activity related to actions across your AWS infrastructure
- Provides event history of your AWS account activity, including actions taken through the AWS Management Console, AWS SDKs, command line tools, API calls, and other AWS services.
- Captures API caller identity, time of API call, source IP address of caller, request params, and returned response elements.
- A trail can be applied to all regions or a single region. As a best practice, create a trail that applies to all regions in the AWS partition in which you are working. This is the default setting when you create a trail in the CloudTrail console.
- Resources:
    - [CloudTrail Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-aws-cloudtrail/)
    - [AWS Cloud Trail](https://aws.amazon.com/cloudtrail/)

## X-Ray
- Helps you debug and analyze your microservices applications with request tracing so you can find the root cause of issues and performance.
- End-to-end view or requests as they travel through an application.
- Especially useful for complex microservice architectures.

