# Messaging

## SQS (Simple Queing Service)
- Managed message queue for decoupling and scaling microservices, distributed systems, and serverless applications.
- Useful to send, store, and receive messages between software components at any volume, without losing messages or requiring other services to be available.
- Uses pull based (polling), not push based.
- Messages in the SQS queue will continue to exist even after the EC2 instance has processed it, until you delete that message.
- You have to ensure that you delete the message after processing to prevent the message from being received and processed again once the visibility timeout expires.
- Queue Types:
    - Standard: Offers maximum hroughput, best-effort ordering, and at-least-once delivery.
    - FIFO: Gurantees that messages are processed exactly once, in the exact order that they are sent.
- Message Consumption Methods:
    - Short Polling: Default method that queries only a subset of its servers (based on a weighted random distribution) to determine whether any messages are available for a response.
    - Long Polling: Reduce your costs while allowing your consumers to receive messages as soon as they arrive in the queue.
- Resources:
    - [SQS Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-sqs/)
    - [SQS Basic Architecture](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-basic-architecture.html)
    - [SQS Message Lifecycle](https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-message-lifecycle.html)

## SNS (Simple Notification Service)
- Resources:
    - 

## MQ (Message Queue)
- Managed message broker service for Apache ActiveMQ.
- Supports industry-standard APIs and protocols so you can switch from any standards-based message broker to Amazon MQ without rewriting the messaging code in your applications.
- Resources:
    - [Amazon MQ Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-mq/)
    - [Amazon MQ FAQs](https://aws.amazon.com/amazon-mq/faqs/)
