# Compute

## Lambda
- Compute service that lets you run code without provisioning or managing servers.
- Can be triggered from HTTP, S3, DynamoDB, Kinesis, SNS, or CloudWatch.
- Features: Environment Variables, ...
- Resources:
    - [Lambda Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-aws-lambda/)
    - [What Is AWS Lambda?](https://docs.aws.amazon.com/lambda/latest/dg/welcome.html)

### Lambda Environment Variables
- Can use Lambda Environment Variables to manage secrets like API keys, usernames, and passwords.
- When you create or update Lambda functions that use environment variables, AWS Lambda encrypts them using the AWS Key Management Service.
- When your Lambda function is invoked, those values are decrypted and made available to the Lambda code.
- The first time you create or update Lambda functions that use environment variables in a region, a default service key is created for you automatically within AWS KMS.
- If you wish to use encryption helpers and use KMS to encrypt environment variables after your Lambda function is created, you must create your own AWS KMS key and choose it instead of the default key.
- Creating your own key gives you more flexibility, including the ability to create, rotate, disable, and define access controls, and to audit the encryption keys used to protect your data.
- Resources:
    - [Using Lambda Environment Variables](https://docs.aws.amazon.com/lambda/latest/dg/configuration-envvars.html#env_encrypt)

## EC2 (Elastic Compute Cloud)
- Partially-managed compute.
- Instances can be combined to optimize workload cost and performance.
- Transferring data from an EC2 instance to Amazon S3, Amazon Glacier, Amazon DynamoDB, Amazon SES, Amazon SQS, or Amazon SimpleDB in the same AWS Region has no cost at all.
- Provides you access to the operating system of the instance that you created.
- Auto Scaling?
- Instances:
    - On-Demand: Pay by the hour, auto scale instances to meet traffic.
    - Reserved (RI): Reserve capaity at a discounted hourly rate.
    - Spot: Get access to cheap compute (up to 90% discount) when AWS has a surplus. Great for stateless, fault-tolerant, or flexible applications such as big data, containerized workloads, CI/CD, web servers, high-performance computing (HPC), and other test & development workloads
- Config:
    - One-Zone-AZ
- Resources:
    - https://tutorialsdojo.com/aws-cheat-sheet-amazon-elastic-compute-cloud-amazon-ec2/

### EC2 Reserved Instance
- Reserve capaity at a discounted hourly rate.
- Configs:
    - Standard: Best for steady state usage, cheapest (75% discount).
    - Convertible: Change attributes as long as >= to value of current instance, affordable (54% discount).
     - Scheduled: Launched within reserved time windows at fraction of day increment.
- Resources:
    - https://aws.amazon.com/ec2/pricing/reserved-instances/

### Self-Monitoring EC2 Instance
- Use an  EIP (Elastic IP) address and then write a custom script that checks the health of the EC2 instance and if the instance stops responding, the script will switch the EIP address to a standby EC2 instance.
- A custom script enables one Amazon Elastic Compute Cloud (EC2) instance to monitor another Amazon EC2 instance and take over a private "virtual" IP address on instance failure.
- Resources:
    - https://aws.amazon.com/articles/leveraging-multiple-ip-addresses-for-virtual-ip-address-fail-over-in-6-simple-steps/

## EC2 Security Groups
- Acts as a firewall.
- It will only control both inbound and outbound traffic at the instance level and not on the whole VPC.
- Resources:
    - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-security-groups.html#security-group-rules

## EC2 Instance Store
- Temporary block-level storage for your instance.
- Ideal for temporary storage of information that changes frequently, such as buffers, caches, scratch data, and other temporary content, or for data that is replicated across a fleet of instances, such as a load-balanced pool of web servers.
- The data in an instance store persists only during the lifetime of its associated instance. 
- If an instance reboots (intentionally or unintentionally), data in the instance store persists. However, data in the instance store is lost if the underlying disk drive fails, the instance stops, or the instance terminates.
- Resources: 
    - [EC2 Instance Store](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/InstanceStorage.html)

## EC2 BYOIP (Bring Your Own IP)
- Can use ROA (Route Origin Authentication) to keep the same IP address when migrating an application.
- Resources:
    - [Bring Your Own IP Addresses](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-byoip.html)

## ECR (Elastic Container Registry)
- Managed Docker container registry.
- Makes it easy for developers to store, manage, and deploy Docker container images.

## ECS (Elastic Container Service)
- Managed container orchestration service for Docker containers.
- With simple API calls, you can launch and stop Docker-enabled applications, query the complete state of your application, and access many familiar features such as IAM roles, security groups, load balancers, Amazon CloudWatch Events, AWS CloudFormation templates, and AWS CloudTrail logs.
- Resources:
    - [ECS Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-elastic-container-service-amazon-ecs/)

## ECS Secrets
- Amazon ECS enables you to inject sensitive data into your containers by storing your sensitive data in either AWS Secrets Manager secrets or AWS Systems Manager Parameter Store parameters and then referencing them in your container definition.
- This feature is supported by tasks using both the EC2 and Fargate launch types.
- To inject sensitive data into your containers as environment variables, use the `secrets` container definition parameter.
- To reference sensitive information in the log configuration of a container, use the `secretOptions` container definition parameter.
- Resources:
    - [Specifying Sensitive Data](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/specifying-sensitive-data.html)

## EKS (Elastic Container Service for Kubernettes)
- Managed containerized apps with Kubernettes.
- Runs the Kubernetes management infrastructure on AWS.

## Fargate
- Serverless compute engine for containers.
- Works with ECS and EKS
- Resources:
    - [Fargate](https://aws.amazon.com/fargate/)

## Parallel Cluster
- AWS-supported open-source cluster management tool that makes it easy for you to deploy and manage High-Performance Computing (HPC) clusters on AWS.
