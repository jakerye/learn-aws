# Pipeline

## SWF (Simple Workflow Service)
- Fully-managed state tracker and task coordinator.
- Helps developers build, run, and scale background jobs that have parallel or sequential steps.
- Resources:
    - https://aws.amazon.com/swf/

## CodeBuild
- Fully-managed build service.
- Compiles source code, runs tests, and produces software packages that are ready to deploy.
- Resources:
    - https://aws.amazon.com/codebuild/

## CodeDeploy
- Fully-managed deploy service.
- Deploy to to a variety of compute services such as Amazon EC2, AWS Fargate, AWS Lambda, and your on-premises servers.
- Resources:
    - https://aws.amazon.com/codedeploy/
    - https://tutorialsdojo.com/aws-cheat-sheet-aws-codedeploy/

## Device Farm
- App testing service that lets you test and interact with your Android, iOS, and web apps on many devices at once, or reproduce issues on a device in real time.
- Resources:
    - [AWS Device Farm](https://aws.amazon.com/device-farm/)

