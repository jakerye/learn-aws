# Network


## CloudFront
- CDN (Content Delivery Network) service that speeds up delivery of (static) content (e.g. images, videos, app binaries) to your customers. 
- Can also be used to distribute other data and APIs.
- Can use Field Level Encryption to securely upload user-submitted sensitive information to your web servers
- Resources:
    - https://tutorialsdojo.com/aws-cheat-sheet-amazon-cloudfront/

### CloudFront Signed Cookies
- Use to control who can access your content.
- Use when you don't want to change your current URLs or when you want to provide access to multiple restricted files.
- Resources:
    - [Choosing Between Signed URLs and Signed Cookies](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-choosing-signed-urls-cookies.html)
    - [Using Signed Cookies](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-signed-cookies.html)

### CloudFront Signed URLs
- Use to control who can access your content.
- Use when you want to restrict access to individual files, your users are using a client that doesn't support cookies, or you are using a CloudFront RTMP (Real-Time Messaging Protocol) distribution.
- Can use a Custom Policy to restrict content by IP address, access time of day, and more.
- Can use to include additional information (e.g. expiration date and time via Canned Policy) to provide more control over access to content.
- Resources:
    - [Choosing Between Signed URLs and Signed Cookies](https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/private-content-choosing-signed-urls-cookies.html)

## API Gateway
- Fully-managed service that makes it easy for developers to create, publish, maintain, monitor, and secure APIs at any scale.
- Manages traffic, auth / access control, monitoring, and versioning.
- API types include: RESTful APIs and WebSocket APIs.
- Features include: Throttling, Cache, ...
- Resources:
    - [API Gateway Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-api-gateway/)
    - [API Gateway FAQs](https://aws.amazon.com/api-gateway/faqs/)

### API Gateway Throttling
- Can enable throttling limits to prevent api from getting overwhelmed.
- Throttling limits can be set for standard rates and bursts. 
- Can be set at multiple levels including global and by a service call.
- API Gateway tracks the number of requests per second.
- Any request over the limit will receive a 429 HTTP response.
- Resources:
    - [API Gateway Throttling & Caching FAQs](https://aws.amazon.com/api-gateway/faqs/#Throttling_and_Caching)

### API Gateway Cache
- Can enable and provision a cache to improves performance and reduce the traffic sent to your back end. 
- Cache settings allow you to control the way the cache key is built and the TTL (time-to-live) of the data stored for each method.
- Resources:
    - [API Gateway Throttling & Caching FAQs](https://aws.amazon.com/api-gateway/faqs/#Throttling_and_Caching)

## Elastic Load Balancer (ELB)
- Automatically distributes incoming application traffic across multiple targets.
- Types include: Application Load Balancer, Network Load Balancer, and Classic Load Balancer
- Features include: Health Checks, ...
- Resources:
    - [Elastic Load Balancing Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-aws-elastic-load-balancing-elb/)
    - [EC2 Instance Health Check vs ELB Health Check vs Auto Scaling and Custom Health Check](https://tutorialsdojo.com/aws-cheat-sheet-ec2-instance-health-check-vs-elb-health-check-vs-auto-scaling-and-custom-health-check-2/)

### ELB Health Checks
- Additional tests provided by the load balancer (in conjunction with Auto Scaling Health Checks).
- The load balancer periodically sends pings, attempts connections, or sends requests to test the EC2 instances. These tests are called health checks.
- Performs health checks to registered targets.
- If any target fails the node is considered unhealthy. 
- When considered unhealthy, the load balancer nodes route request across its unhealthy targets.
- The load balancer resumes routing requests to the instance when it has been restored to a healthy state.
- Resources:
    - [Configure Health Checks for Your Classic Load Balancer](https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/elb-healthchecks.html)
    - [Adding Elastic Load Balancing Health Checks to an Auto Scaling Group](https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-add-elb-healthcheck.html)

### ELB Cross-Zone Load Balancing
- ELB feature.

## Auto Scaling
- When a scale-in policy has been triggered due to the low number of incoming traffic to the application, the EC2 instance launched from the oldest config will be terminated.
- Resources:
    - [Auto Scaling Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-aws-auto-scaling/)

### Auto Scaling Health Checks
- The default health checks for an Auto Scaling group are EC2 status checks only.
- If an instance fails these status checks, the Auto Scaling group considers the instance unhealthy and replaces.
- If one load balancer reports an instance as unhealthy, the Auto Scaling group replaces the instance, even if other load balancers report it as healthy.
- Can optionally configure to use Elastic Load Balancing health checks to ensure that the group can determine an instance's health based on additional tests provided by the load balancer.
- If you configure the Auto Scaling group to use Elastic Load Balancing health checks, it considers the instance unhealthy if it fails either the EC2 status checks or the load balancer health checks.
- If you attach multiple load balancers to an Auto Scaling group, all of them must report that the instance is healthy in order for it to consider the instance healthy.
- If one load balancer reports an instance as unhealthy, the Auto Scaling group replaces the instance, even if other load balancers report it as healthy.
- Resources:
    - [Adding Elastic Load Balancing Health Checks to an Auto Scaling Group](https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-add-elb-healthcheck.html)

## VPC (Virtual Private Cloud)
- A virtual network dedicated to an AWS account.
- Can modify the Network ACL (Access Control List) to set an additional layer of security that acts as a firewall for controlling traffic in and out of one or more subnets.
- Can use Flow Logs to capture information about the IP traffic going to and from network interfaces in your VPC.
- By default uses Audit Logging to log information about connections and user activities in your database.
- Resources:
    - [VPC Cheat Sheet](https://tutorialsdojo.com/aws-cheat-sheet-amazon-vpc/)

### VPC Subnets
- Total number of IP addresses of a given CIDR Block = 2 ^ ( 32 - Mask Number ). For CIDR Block 172.0.0.0/28, 28 is the Mask Number.
- The CIDR block must not overlap with any existing CIDR block that's associated with the VPC.
- You cannot increase or decrease the size of an existing CIDR block.
- The CIDR block must not be the same or larger than the CIDR range of a route in any of the VPC route tables. 
- Resources:
    - [VPCs and Subnets](https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html)

## NACL (Network Access Control List)
- Use to control traffic coming in and out of your VPC network.
- Optional layer of security for your VPC that acts as a firewall for controlling traffic in and out of one or more subnets.
- Resources:
    - [Network ACLs](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-network-acls.html)


## EIP (Elastic IP) Address
- Static IPv4 address designed for dynamic cloud computing.
- Can use to mask the failure of an instance or software by rapidly remapping the address to another instance in your account.
- Resources:
    - https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/elastic-ip-addresses-eip.html
